CREATE TABLE "Etiketa" (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`opis`	TEXT NOT NULL,
	`boja`	TEXT NOT NULL,
	`oznaka`	TEXT NOT NULL UNIQUE
);
CREATE TABLE "Spomenik_has_etiketa" (
	`spomenik_id`	INTEGER,
	`etiketa_id`	INTEGER,
	PRIMARY KEY(spomenik_id,etiketa_id),
	FOREIGN KEY(`spomenik_id`) REFERENCES `Spomenik`(`id`),
	FOREIGN KEY(`etiketa_id`) REFERENCES `Etiketa`(`id`)
);
CREATE TABLE "Tip" (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`ime`	TEXT NOT NULL,
	`ikonica`	TEXT NOT NULL,
	`opis`	TEXT,
	`oznaka`	TEXT NOT NULL UNIQUE
);
CREATE TABLE "Spomenik" (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`ime`	TEXT NOT NULL,
	`opis`	TEXT,
	`tip`	INTEGER,
	`era`	TEXT NOT NULL,
	`ikonica`	TEXT,
	`obradjen`	INTEGER NOT NULL,
	`naseljen`	INTEGER NOT NULL,
	`datum`	TEXT NOT NULL,
	`prihod`	REAL NOT NULL,
	`unesco`	INTEGER NOT NULL,
	`status`	TEXT NOT NULL,
	`oznaka`	TEXT NOT NULL UNIQUE,
	`X`	REAL DEFAULT -1,
	`Y`	REAL DEFAULT -1,
	FOREIGN KEY(`tip`) REFERENCES `Tip`(`id`)
);
